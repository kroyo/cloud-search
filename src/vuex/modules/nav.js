const nav = {
  state : {
    itemLists : [],
    choosed :[],//需要session保存
    boxIndex : 0,//需要session保存
    advitemId : 2,
    advBoxshow : false,
    tabName :'',//selectSearch
  },
  mutations : {
    clearItemListBtn(state,tab){
      state.itemLists.forEach(function(elem){
        if(elem.name === tab && elem.messages){
          elem.messages.forEach(function(item){
            item.active = false;
          })
        }
      })
    },
    saveItemList(state,res) {
      state.itemLists = res;
      let JSONdata = JSON.stringify(res);
      window.sessionStorage.setItem('homeInfo',JSONdata);
    },
    saveChoosed(state,val) {
      if(val.length === 0){
        // state.choosed.splice(0,state.choosed.length);   // kroyo
        state.choosed = [];
      }
      state.choosed = val;
      let JSONdata = JSON.stringify(state.choosed);
      window.sessionStorage.setItem('choosed',JSONdata);
    },
    saveBoxindex(state,res) {
      state.boxIndex = res;
      let JSONdata = JSON.stringify(state.boxIndex);
      window.sessionStorage.setItem('boxIndex',JSONdata);
    },
    savetabName(state,val) {
      state.tabName = val;
    },
    setAdvitemId(state) {
      state.advitemId += 1;
    },
    setadvBoxshow(state,val) {
      state.advBoxshow = !state.advBoxshow;
    }
  },
  actions : {

  }
}

export default nav
