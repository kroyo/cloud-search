import Vue from 'vue'
const collection = {
  state : {
    collectedList : {},
  },
  mutations : {
    saveCollectedList (state,data){
      data.forEach((elem,index) => {
        // state.collectedList.push(elem.userId);
        Vue.set(state.collectedList,elem.code,elem.name);
      })
    },
    removeCollectedItem(state,id){
      Vue.delete(state.collectedList,id);
      // let index = state.collectedList.indexOf(id);
      // state.collectedList.splice(index,1);
    },
    setCollectedItem(state,item){
      let _name;
      for(let key in item.source){
        if(item.source[key].split(': ')[0] === '姓名'){
          _name = item.source[key].split(': ')[1].replace(/<em>/,'').replace(/<\/em>/,'');
        }
      }
        Vue.set(state.collectedList,item.id,_name);
    },
  }
}
export default collection
