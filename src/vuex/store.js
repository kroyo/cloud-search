import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import user from './modules/user'
import nav from './modules/nav'
import adv from './modules/adv'
import collection from './modules/collection'
import search from './modules/searchData'

Vue.use(Vuex);

export default new Vuex.Store({
  modules : {
    user,
    nav,
    search,
    adv,
    collection,
  },
  actions,
  getters,
})
