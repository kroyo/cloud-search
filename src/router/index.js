import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const routes = [
  { path: '/404', component: () => import('@/components/errorPage/404.vue'), hidden: true },
  {
    path : '/',
    name : 'home',
    meta : {
      requireAuth : true
    },
    component : () => import('@/components/home.vue')
  },
  {
    path : '/result',
    name : 'result',
    meta : {
      requireAuth : true
    },
    component : () => import('@/components/result.vue')
  },
  {
    path:'/singletable/:res',
    name : 'singleTable',
    props : true,
    meta : {
      requireAuth : true
    },
    component : () => import('@/components/singleTable/singleTable.vue')
  },
  {
    path:'/details/:id',
    name : 'details',
    props : true,
    mete : {
      requireAuth : true
    },
    component : () => import('@/components/details/details.vue')
  },
    { path: '*', redirect: '/404', hidden: true }
];

export default new Router({
      // mode : 'history',
      routes,
})
