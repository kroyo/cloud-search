// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import store from './vuex/store'
import { Pagination ,Button, Select, Option,DatePicker,
         Input , Menu, MenuItem ,Submenu,Tabs,TabPane,Tag, Loading
        ,Table, TableColumn,Popover,Upload,Alert,Checkbox} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './util/directive'
// import '@/permission' // permission control

Vue.use(Vuex);
Vue.use(Pagination);
Vue.use(Button);
Vue.use(Select);
Vue.use(Option);
Vue.use(Input);
Vue.use(Menu);
Vue.use(Tabs);
Vue.use(Upload);
Vue.use(Tag);
Vue.use(Popover);
Vue.use(Table);
Vue.use(Alert);
Vue.use(TableColumn);
Vue.use(Loading);
Vue.use(TabPane);
Vue.use(MenuItem);
Vue.use(DatePicker);
Vue.use(Submenu);
Vue.use(Checkbox);
Vue.prototype.$axios = axios;
Vue.config.productionTip = false;
//改写date内置方法
Date.prototype.toLocaleString = function() {
        return this.getFullYear() + "年" + (this.getMonth() + 1) + "月" + this.getDate() + "日 " + this.getHours() + ":" + this.getMinutes() + ":" + this.getSeconds();
    };
Date.prototype._tolocalString = function(){
  return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate();
}
/* eslint-disable no-new */
let vm = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
