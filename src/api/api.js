/* jshint esversion: 6 */
import axios from 'axios';
const baseURL = process.env.NODE_ENV === 'development' ? 'http://g.com' : '';
// const mockURL = 'http://g.com';
const CORSUrl = '/api';
const Qs = require('qs');

//axios拦截器
axios.interceptors.request.use(
 config => {
     let _token = document.cookie.match(/token=([\S\s]{32})/) ? document.cookie.match(/token=([\S\s]{32})/)[1] : null;
     if (_token) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
         config.headers.token = `${_token}`;
     }
     return config;
 },
 err => {
     return Promise.reject(err);
 });

//api管理
// 用户信息管理
export const userInfo = () => {return axios.post(baseURL+'/user/info').then((res) => {
 if(res.status == 200){
   return res;
 }else if(res.status == 302){
   window.location.href = res.data.portal;
 }
}).catch((error) => {
   console.log('出错',error.response.status);
   if(error.response.status == 302){
     window.location.href = error.response.data.portal;
   }
 });
};
export const homeInfo = () => {return axios.post(baseURL+'/categories').then((res) => {
 if(res.status == 200){
   return res;
 }else if(res.status == 302){
   window.location.href = res.data.portal;
 }
}).catch((error) => {
   console.log('出错',error.response.status);
   if(error.response.status == 302){
     window.location.href = error.response.data.portal;
   }
 });
};


//搜索api管理
export const apiDictionary = () => {return axios.post(baseURL+'/kw/dictionaries').then((res) => {
  if(res.status == 200){
    return res;
  }else if(res.status == 302){
    window.location.href = res.data.portal;
  }
 }).catch((error) => {
    console.log('出错',error.response.status);
    if(error.response.status == 302){
      window.location.href = error.response.data.portal;
    }
  });
};
export const apiResults = (param,apiName) => {
 return axios({
   method:'post',
   url : baseURL + '/kics/search/'+ apiName,
   headers : {
     'Content-Type' : 'application/x-www-form-urlencoded'
   },
   transformRequest: [data => Qs.stringify(data)],
 data : param,
}).then((res) => res)
.catch((error) => {
   console.log('出错',error.response.status);
   if(error.response.status == 302){
     window.location.href = error.response.data.portal;
   }
 });
};

export const apiPersonInfo = (param,type,id) => {
 return axios({
   method : 'post',
   url : baseURL + '/kics/ext/' + type +'/' + id,
   headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
   transformRequest : [data => Qs.stringify(data)],
   data : param,
 }).then((res)=> res);
};

export const apiHistory = () => {return axios.post(baseURL+'/histories')};
export const apiUpload = (param,apiName) => {
 return axios({
   method : 'post',
   url : baseURL + '/upload/'+ apiName,
   headers : {
     'Content-Type' : 'application/x-www-form-urlencoded'
   },
   transformRequest : [data => Qs.stringify(data)],
   data : param,
 }).then((res)=> res)
 .catch((error) => {
     console.log('出错',error.response.status);
     if(error.response.status == 302){
       window.location.href = error.response.data.portal;
     }
   });
};

//全国人口查询
export const qgrkSearch = (indexCode,id) => {
 return axios({
   method:'post',
   url : baseURL + '/kics/citizen/'+ indexCode + '/' + id,
}).then((res) => res)
.catch((error) => {
   console.log('出错',error.response.status);
   if(error.response.status == 302){
     window.location.href = error.response.data.portal;
   }
 });
};

//收藏api
export const apiCollection = () => {return axios.post(baseURL + '/collections').then((res) => {
  if(res.status == 200){
    return res;
  }else if(res.status == 302){
    window.location.href = res.data.portal;
  }
 }).catch((error) => {
    console.log('出错',error.response.status);
    if(error.response.status == 302){
      window.location.href = error.response.data.portal;
    }
  });
};
// export const apiCollected = () => {
//   return axios({
//
//   })
// }

//高级搜索模型api
export const apiAdvModel = () => {return axios.get(baseURL + '/search/models').then((res) => {
  if(res.status == 200){
    return res;
  }else if(res.status == 302){
    window.location.href = res.data.portal;
  }
 }).catch((error) => {
    console.log('出错',error.response.status);
    if(error.response.status == 302){
      window.location.href = error.response.data.portal;
    }
  });
};
export const apiSetAdvModel = (param) => {
 return axios({
   method : 'post',
   url : baseURL + '/search/model',
   transformRequest : [data => Qs.stringify(data)],
   data : param,
 }).then((res)=> res);
};
export const apiDeleteAdvModel = (id) => {
 return axios({
   method : 'delete',
   url : baseURL + '/search/model/'+id,
 }).then((res)=> res);
};

//登出api
export const apiLogout = () => {return axios.post(baseURL+'/logout').then((res) =>{
 if(res.status == 302){
   window.location.href = res.data.portal;
 }else {
   return res;
 }
}).catch(function(error){
   if(error.response.status == 302) {
     window.location.href = error.response.data.portal;
   }
})}

//获取系统时间
export const apiSystemTime = () => {return axios.post(baseURL + '/system/time')}
