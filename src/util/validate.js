export function personIdentification (val){
//二代检验
const identifyExp = /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|[xX])$/;
//一代二代共同校验
const allIdentifyExp = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/
//15或18位检验
const _15or18 = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
//证件认证
const certificate = /^\w{6,18}$/;
return certificate.test(val);
}
