import Mock from 'mockjs';
Mock.setup({
    timeout: '200-500'
})
Mock.mock('http://g.com/categories',
[
{
  "code": "all",
  "indices":[
    {"code":"rymn","remarks":"2018年12月"},
    {"code":"rymn2","remarks":"2017"},
    {"code":"rymn3","remarks":"2016"}
  ],
  "name": "全部",
  "seqNum": 10
},
{
  "code": "ry",
  "indices": [
      {"code":"rymn","remarks":"2018年12月"},
      {"code":"rymn2","remarks":"2017"},
      {"code":"rymn3","remarks":"2016"}
    ],
  "name": "人员",
  "seqNum": 20
},
{
  "code": "aj",
  "indices": [],
  "name": "案件",
  "seqNum": 30
},
{
  "code": "wp",
  "indices": [
      {"code":"rymn10","remarks":"2018年11月"},
      {"code":"rymn2","remarks":"2017"},
    ],
  "name": "物品",
  "seqNum": 40
},
// {
//   "code": "dz",
//   "indices": null,
//   "name": "地址",
//   "seqNum": 50
// },
// {
//   "code": "zz",
//   "indices": null,
//   "name": "组织",
//   "seqNum": 60
// }
]);

Mock.mock('http://g.com/user/info',
{
  "id": "10",
  "account": "admin",
  "cardNo": "430421199401246137",
  "name": "系统管理员",
  "secret": 3,
  "orgId": "root",
  "orgName": "组织机构",
  "otherPermissions": {
    "super": "super",
    "citizen": "citizen",
    "citizenxp": "citizenxp",
    "detail": "detail"
  },
  "btnPermissions": [{
    "indexCode": "rygz_n",
    "name": "详细信息",
    "code": "detail",
    "url": "http://192.168.0.181:8581/kingyea-superdocument/view/person/_id_?token=_token_"
  }, {
    "indexCode": "rygz_n",
    "name": "全国人口",
    "code": "citizen"
  }, {
    "indexCode": "rygz_n",
    "name": "人口图片",
    "code": "citizenxp",
    "url": "http://68.202.4.5:8686/yj/citizen/image/_idno_/XP"
  }, {
    "indexCode": "clgz_n",
    "name": "详细信息",
    "code": "detail",
    "url": "http://192.168.0.181:8581/kingyea-superdocument/view/wp/_id_?token=_token_"
  }, {
    "indexCode": "clgz_n",
    "name": "人口图片",
    "code": "citizenxp",
    "url": "http://68.202.4.5:8686/yj/citizen/image/_idno_/WP"
  }, {
    "indexCode": "_all",
    "name": "高级查询",
    "code": "super"
  }],
  "resources": [{
    "code": "ry",
    "name": "人员",
    "seqNum": 20,
    "resources": [{
      "code": "ccxx",
      "name": "乘车信息",
      "keywords": [{
        "id": "kw_99999",
        "name": "乘车日期",
        "selType": "time"
      }]
    }, {
      "code": "czrk",
      "name": "常住人口",
      "keywords": [{
        "id": "kw_77777",
        "name": "出生日期",
        "selType": "time"
      }, {
        "id": "kw_22222",
        "name": "性别",
        "selType": "select"
      }, {
        "id": "kw_33333",
        "name": "姓名",
        "selType": "input"
      }, {
        "id": "kw_44444",
        "name": "户籍",
        "selType": "input"
      }, {
        "id": "kw_55555",
        "name": "民族",
        "selType": "input"
      }, {
        "id": "kw_66666",
        "name": "籍贯",
        "selType": "input"
      }]
    }]
  }, {
    "code": "wp",
    "name": "物品",
    "seqNum": 40,
    "resources": [{
      "code": "jdcbglsjl",
      "name": "机动车变更历史记录信息表",
      "keywords": []
    }, {
      "code": "jdcjbxx",
      "name": "机动车信息表",
      "keywords": []
    }]
  }],
  "resourcePermissions": {
    "ry": ["ccxx", "gnlk", "czrk"],
    "wp": ["jdcjbxx", "jdcbglsjl"],
    "aj": ["aj"]
  }
}
);

Mock.mock('http://g.com/collections',
{
  "pageNum": 1,
  "pageSize": 2,
  "size": 2,
  "startRow": 1,
  "endRow": 2,
  "total": 3,
  "pages": 2,
  "list": [{
    "id": "col_000",
    "code": "360781198404133622",
    "time": 1526654435000,
    "type": "09",
    "name": "测试1",
    "label": "12",
    "status": "1",
    "userId": "admin"
  }, {
    "id": "col_111",
    "code": "456",
    "time": 1526654455000,
    "type": "08",
    "name": "测试2",
    "label": "45",
    "status": "1",
    "userId": "admin"
  }],
  "prePage": 0,
  "nextPage": 2,
  "isFirstPage": true,
  "isLastPage": false,
  "hasPreviousPage": false,
  "hasNextPage": true,
  "navigatePages": 8,
  "navigatepageNums": [1, 2],
  "navigateFirstPage": 1,
  "navigateLastPage": 2,
  "firstPage": 1,
  "lastPage": 2
}
)
Mock.mock('http://g.com/kw/dictionaries',
{
  "性别": [{
    "code": "male",
    "name": "男",
    "seqNum": 1
  }, {
    "code": "female",
    "name": "女",
    "seqNum": 2
  }]
});
// Mock.mock('http://g.com/search/ry',{
//   "hits": [
//     {
//       "highlight": {},
//       "id": "330227192459753549",
//       "source": {},
//       "type": "ryxx"
//     },
//     { "highlight": {},
//       "id": "3302271939204988",
//       "source": {},
//       "type": "ryxx"
//     },
//     // { "highlight": {},
//     //   "id": "3302271939204988",
//     //   "source": {},
//     //   "type": "ryxx"
//     // },
//     // { "highlight": {},
//     //   "id": "3302271939204988",
//     //   "source": {},
//     //   "type": "ryxx"
//     // }
//   ],
//   "took":5,
//   "total":1001,
//   "my_zys":""
// });

Mock.mock('http://g.com/kics/search/ry', {
  "hits": [{
    "id": "360981190328925019",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 孔青",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981190328925019",
      "9998": "出生日期: 1992-11-10",
      "null": "火车车次: K8718",
      "500": "乘车日期: 2017-05-01"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 15
    }
  }, {
    "id": "360981190462042722",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 云鸳",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981190462042722",
      "9998": "出生日期: 1991-03-24",
      "null": "旅馆房间号: 010619",
      "500": "乘车日期: 2017-04-28"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 13
    }
  }, {
    "id": "360981190647851626",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 史磊",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省龙南县",
      "110": "公民身份号码: 360981190647851626",
      "9998": "出生日期: 1969-03-16",
      "null": "火车车次: K8718",
      "500": "乘车日期: 2016-12-11"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 21
    }
  }, {
    "id": "360981191495550012",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 柏力",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981191495550012",
      "9998": "出生日期: 1993-03-13",
      "null": "旅馆房间号: 539",
      "500": "乘车日期: 2017-05-04"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 82
    }
  }, {
    "id": "36098119174590242X",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 韦赛玲",
      "400": "户籍: 江西省南丰县",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 36098119174590242X",
      "9998": "出生日期: 1971-08-06",
      "null": "火车车次: D6226",
      "500": "乘车日期: 2017-04-23"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 11
    }
  }, {
    "id": "360981191802164512",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 陈国庆",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981191802164512",
      "9998": "出生日期: 1963-12-12",
      "null": "旅馆房间号: 010409",
      "500": "乘车日期: 2017-08-16"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 5
    }
  }, {
    "id": "360981191880741129",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 吴成",
      "400": "户籍: 江西省星子县",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981191880741129",
      "9998": "出生日期: 1990-06-15",
      "null": "火车车次: D6505",
      "500": "乘车日期: 2017-10-07"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 30
    }
  }, {
    "id": "360981192092282821",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 严雅丽",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981192092282821",
      "9998": "出生日期: 1998-07-04",
      "null": "火车车次: K8717",
      "500": "乘车日期: 2017-07-03"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 15
    }
  }, {
    "id": "36098119256108602X",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 褚小敏",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 36098119256108602X",
      "9998": "出生日期: 1997-08-06",
      "null": "火车车次: K8728",
      "500": "乘车日期: 2017-01-02"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 23
    }
  }, {
    "id": "360981194181630119",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 任嘉友",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981194181630119",
      "9998": "出生日期: 1995-02-03",
      "null": "旅馆房间号: 601",
      "500": "乘车日期: 2017-04-29"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 16
    }
  }, {
    "id": "36098119424284132X",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 唐凌燕",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 36098119424284132X",
      "9998": "出生日期: 1998-09-04",
      "null": "火车车次: D3246",
      "500": "乘车日期: 2017-02-05"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 22
    }
  }, {
    "id": "360981194793667035",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 曹垚智",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981194793667035",
      "9998": "出生日期: 1976-01-13",
      "null": "旅馆房间号: 706",
      "500": "乘车日期: 2017-08-08"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 3
    }
  }, {
    "id": "360981194880742516",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 酆小兵",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981194880742516",
      "9998": "出生日期: 1992-10-06",
      "null": "火车车次: K527",
      "500": "乘车日期: 2017-01-16"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 17
    }
  }, {
    "id": "360981195186946111",
    "source": {
      "404": "性别: 男性",
      "102": "姓名: 汤宝辉",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981195186946111",
      "9998": "出生日期: 1985-05-15",
      "null": "火车车次: T171",
      "500": "乘车日期: 2017-01-26"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 8
    }
  }, {
    "id": "360981197634020146",
    "source": {
      "404": "性别: 女性",
      "102": "姓名: 郑颂琴",
      "400": "户籍: 江西省丰城市",
      "200": "民族: 汉族",
      "201": "籍贯: 江西省丰城市",
      "110": "公民身份号码: 360981197634020146",
      "9998": "出生日期: 1989-11-03",
      "null": "火车车次: K1236",
      "500": "乘车日期: 2017-04-14"
    },
    "type": "rygz_n",
    "trackData": {
      "乘车信息": 5
    }
  }],
  "took": 78,
  "total": 2754
});

Mock.mock('http://g.com/kics/search/ry/rymn',{
  "hits": [
    {
      "highlight": {},
      "id": "360781198404133622",
      "source": {
        "104": "性别: 女性",
        "102": "姓名: <em>王</em>伟哦",
        "200": "民族: 汉族",
        "300": "籍贯: 江西省瑞金市(老)",
        "9998": "出生日期: 450633600000"
      },
      "type": "ry_mn"
    },
    {
      "highlight": {},
      "id": "360731201504105320",
      "source": {
        "104": "性别: 女性",
        "102": "姓名: <em>王</em>璐佳",
        "200": "民族: 汉族",
        "300": "籍贯: 江西省于都县",
        "9998": "出生日期: 1428595200000"
      },
      "type": "rygz"
    },
    {
      "highlight": {},
      "id": "361002200411242242",
      "source": {
        "104": "性别: 女性",
        "102": "姓名: 周慧",
        "200": "民族: 汉族",
        "300": "籍贯: 江西省抚州市临川区",
        "9998": "出生日期: 1101225600000"
      },
      "type": "ry_mn"
    },
    {
      "highlight": {},
      "id": "445381199303097228",
      "source": {
        "104": "性别: 女性",
        "102": "姓名: <em>王</em>洪丽",
        "200": "民族: 汉族",
        "300": "籍贯: 广东省罗定市",
        "9998": "出生日期: 731606400000"
      },
      "type": "rygz"
    },
    {
      "highlight": {},
      "id": "360103195211212716",
      "source": {
        "104": "性别: 男性",
        "102": "姓名: <em>王</em>国华",
        "400": "户籍: 上海市青浦区",
        "200": "民族: 汉族",
        "300": "籍贯: 江苏省丹阳市",
        "110": "公民身份号码: 360103195211212716",
        "9998": "出生日期: -540028800000"
      },
      "type": "test"
    },
    {
      "highlight": {},
      "id": "360121197411034426",
      // "source": {
      //   "104": "性别: 女性",
      //   "102": "姓名: <em>王</em>海是",
      //   "400": "户籍: 浙江省永嘉县",
      //   "200": "民族: 汉族",
      //   "300": "籍贯: 浙江省永嘉县",
      //   "110": "公民身份号码: 360121197411034426",
      //   "9998": "出生日期: 152640000000"
      // },
      "type": "rygz"
    }
  ],
  "took": 20,
  "total": 300
});


Mock.mock('http://g.com/search/all',{
  "hits": [
    {
      "highlight": {},
      "id": "330227192459753549",
      "source": {},
      "type": "ryxx"
    },
    { "highlight": {},
      "id": "3302271939204988",
      "source": {},
      "type": "ryxx"
    },
  ],
  "took":5,
  "total":1001,
  "my_zys":""
});
Mock.mock('http://g.com/kics/ext/rygz/360121197411034426',
  {
    "highlight": {},
    "id": "360121197411034426",
    "source": {
      "300": "户籍: 江苏省如东县",
      "400": "籍贯: 江苏省如东县",
      "103": "民族: 汉族",
      "102": "性别: 女性",
      "101": "姓名: <em>卫</em>蔚娜",
      "200": "公民身份号码: 330227192459753549"
    },
    "type": "ry_mn"
  });
  Mock.mock('http://g.com/kics/ext/rygz/3302271939204988',
    {
      "highlight": {},
      "id": "3302271939204988",
      "source": {
        "300": "户籍: 广东省广州市",
        "400": "籍贯: 广东省广州市",
        "103": "民族: 汉族",
        "102": "性别: 男性",
        "101": "姓名: 友测试",
        "200": "公民身份号码: 3302271939204988"
      },
      "type": "rygz"
    });

Mock.mock('http://g.com/histories',
{
  "pageNum": 1,
  "pageSize": 15,
  "size": 2,
  "startRow": 1,
  "endRow": 2,
  "total": 2,
  "pages": 1,
  "list": [{
    "id": "bb0e23877d65410293de71713e2f8472",
    "operateTime": 1524814917000,
    "operateResult": 1,
    "operateName": "kics:search",
    "operateCondition": "{\"q\":\"\",\"indices\":\"rymn\",\"size\":15,\"zys\":\"\",\"ys\":\"ry\",\"from\":0}"
  }, {
    "id": "e442a1b63227460eac91ae0c407ce1f0",
    "operateTime": 1524814917000,
    "operateResult": 1,
    "operateName": "kics:search",
    "operateCondition": "{\"q\":\"货票:123123\",\"indices\":\"rymn2\",\"size\":30,\"zys\":\"\",\"ys\":\"wp\",\"from\":0}"
  }],
  "prePage": 0,
  "nextPage": 0,
  "isFirstPage": true,
  "isLastPage": true,
  "hasPreviousPage": false,
  "hasNextPage": false,
  "navigatePages": 8,
  "navigatepageNums": [1],
  "navigateFirstPage": 1,
  "navigateLastPage": 1,
  "firstPage": 1,
  "lastPage": 1
}
);

Mock.mock('http://g.com/upload/ry',{
  "code": "200",
  "data": {
    "search": 1,
    "unfounds": [111,2],
    "timeSpent": 19,
    "count": 10,
    "url": "/download/20180425/de31b7e58bc74aa088dd6e9474166345.xls"
  }
});

Mock.mock('http://g.com/search/models',
  [{
    "id": "aa",
    "userCode": "admin",
    "kwIds": "ejfidnske12n3n58fk2n8f8dosed&ejfidnske12n3n58fk29downie02ksid&ejfidnske12n3n58fk29down32fs2asw",
    "time": 1527000187000
  }, {
    "id": "bb",
    "userCode": "admin",
    "kwIds": "ejfidnske12n3n58fk29down8v9aosn3|ejfidnske12n3n58fk29downvos8n2na&ejfidnske12n3n58fk29down39dioos2",
    "time": 1527000211000
  }]
);
Mock.mock('http://g.com/search/model',{
  msg : 'ok'
});
Mock.mock('http://g.com/search/model/aa',{
  msg : 'ok'
});
Mock.mock('http://g.com/system/time',1533278011980)
export default Mock;
