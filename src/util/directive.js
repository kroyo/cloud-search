import Vue from 'vue';

Vue.directive('drag',//自定义指令
      {bind:function (el, binding) {
              let oDiv = el;   //当前元素
              oDiv.onmousedown = function (e) {
                if(e.target.className === 'top' || e.target.className === 'el-tabs__nav-scroll'){
                  //鼠标按下，计算当前元素距离可视区的距离
                     let disX = e.clientX - oDiv.offsetLeft;
                     let disY = e.clientY - oDiv.offsetTop;

                     document.onmousemove = function (e) {
                       el.style.cursor = 'move';
                       //通过事件委托，计算移动的距离
                         let l = e.clientX - disX;
                         let t;
                         if(e.clientY - disY <= 0){
                           t = 0;
                         }else {
                           t = e.clientY - disY;
                         }
                       //移动当前元素
                         oDiv.style.left = l + 'px';
                         oDiv.style.top = t + 'px';
                     };
                     document.onmouseup = function (e) {
                         el.style.cursor = 'default';
                         document.onmousemove = null;
                         document.onmouseup = null;
                      };
                }
              };
          },
      }
  );
